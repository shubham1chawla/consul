package com.bfm.app.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name = "find_by_email", query = "select u from Users u where u.email = :email")
@NamedQuery(name = "find_by_username", query = "select u from Users u where u.username = :username")
public class Users {

	@Id
	@GeneratedValue
	private Integer id;
	private String username, email, password;

	public Users() {}
	
	public Users(String username, String email, String password) {
		this.username = username;
		this.email = email;
		this.password = password;
	}
	
	public Users(Integer id, String username, String email, String password) {
		this.id = id;
		this.username = username;
		this.email = email;
		this.password = password;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Users [id=" + id + ", username=" + username + ", email=" + email + ", password=" + password + "]";
	}
}
