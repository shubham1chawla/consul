package com.bfm.app.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bfm.app.dao.TickerDao;
import com.bfm.app.dto.QuandlDTO;
import com.bfm.app.entity.Ticker;
import com.bfm.app.service.QuandlFetch;
import com.bfm.app.util.ConsulErrors;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class APIController {

	private final Integer DEFAULT_START_ROW = 0;
	private final Integer DEFAULT_COUNT_ROW = 15;
	
	@Autowired
	private TickerDao tickerDao;
	
	@Autowired
	private QuandlFetch quandlFetch;
	
	@GetMapping("/tickers")
	public ResponseEntity<List<Ticker>> getTickers(@RequestParam(value = "start", required = false) Integer start,
												   @RequestParam(value = "count", required = false) Integer count) {
		
		List<Ticker> tickers = tickerDao.getTickers((start != null) ? start : DEFAULT_START_ROW, (count != null) ? count : DEFAULT_COUNT_ROW);
		return new ResponseEntity<List<Ticker>>(tickers, HttpStatus.OK);
	}
	
	@GetMapping("/tickers/search")
	public ResponseEntity<List<Ticker>> getTickersViaQuery(@RequestParam(value = "query", required = false) String query) {
		if(StringUtils.isEmpty(query)) {
			return this.getTickers(DEFAULT_START_ROW, DEFAULT_COUNT_ROW);
		}
		List<Ticker> tickers = tickerDao.getTickersLike(query);
		return new ResponseEntity<List<Ticker>>(tickers, HttpStatus.OK);
	}
	
	@GetMapping("/tickers/{code}")
	public ResponseEntity<QuandlDTO> getTickerInfo(@PathVariable(value = "code", required = false) String code) {
		QuandlDTO quandlData = new QuandlDTO();	
		if(StringUtils.isEmpty(code)) {
			quandlData.setError(ConsulErrors.EMPTY_CODE_ERROR);
			return new ResponseEntity<QuandlDTO>(new QuandlDTO(), HttpStatus.BAD_REQUEST);
		}
	
		try {
			quandlData = quandlFetch.getQuandlData(code);
			return new ResponseEntity<QuandlDTO>(quandlData, HttpStatus.OK);
		} catch (IOException e) {
			quandlData.setError(ConsulErrors.UNEXPECTED_EXCEPTION);
			return new ResponseEntity<QuandlDTO>(quandlData, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/tickers/count")
	public ResponseEntity<Long> getTickersCount() {
		Long count = tickerDao.getTickersCount();
		return new ResponseEntity<Long>(count, HttpStatus.OK);
	}
}
