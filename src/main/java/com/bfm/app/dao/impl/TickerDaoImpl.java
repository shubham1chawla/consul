package com.bfm.app.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.bfm.app.dao.TickerDao;
import com.bfm.app.entity.Ticker;

@Repository
@Transactional
public class TickerDaoImpl implements TickerDao {

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public List<Ticker> getTickers(Integer startIndex, Integer count) {
		List<Ticker> res = em.createNamedQuery("GET_TICKERS", Ticker.class).setFirstResult(startIndex).setMaxResults(count).getResultList();
		if(CollectionUtils.isEmpty(res)) {
			return new ArrayList<Ticker>();
		}
		return res;
	}

	@Override
	public List<Ticker> getTickersLike(String query) {
		List<Ticker> res = em.createNamedQuery("SEARCH_QUERY",Ticker.class).setParameter("query", "%"+query+"%").getResultList();
		if(CollectionUtils.isEmpty(res)) {
			return new ArrayList<Ticker>();
		}
		return res;
	}

	@Override
	public Ticker getTickerByCode(String code) {
		return em.createNamedQuery("GET_BY_CODE", Ticker.class).setParameter("code", code).getSingleResult();
	}

	@Override
	public Long getTickersCount() {
		Long res = (Long) em.createQuery("SELECT COUNT(*) FROM Ticker t").getSingleResult();
		if(res != null) {		
			return res;
		}
		return 0l;
	}

}
