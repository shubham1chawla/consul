package com.bfm.app.dao;

import java.util.List;

import com.bfm.app.entity.Ticker;

public interface TickerDao {
	public Ticker getTickerByCode(String code);
	public List<Ticker> getTickers(Integer startIndex, Integer count);
	public List<Ticker> getTickersLike(String query);
	public Long getTickersCount();
}
