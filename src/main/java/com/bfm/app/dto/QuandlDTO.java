package com.bfm.app.dto;

import java.util.Date;

import com.bfm.app.util.ConsulErrors;

public class QuandlDTO {

	private String name, quandlCode;
	private Date date;
	private double open, high, low, close, wap, noOfShares, noOfTrades, totalTurnover, deliverableQuantity,
			perDelQuanToTrade, spreadHighLow, spreadCloseOpen;
	
	private ConsulErrors error;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getQuandlCode() {
		return quandlCode;
	}

	public void setQuandlCode(String quandlCode) {
		this.quandlCode = quandlCode;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getOpen() {
		return open;
	}

	public void setOpen(double open) {
		this.open = open;
	}

	public double getHigh() {
		return high;
	}

	public void setHigh(double high) {
		this.high = high;
	}

	public double getLow() {
		return low;
	}

	public void setLow(double low) {
		this.low = low;
	}

	public double getClose() {
		return close;
	}

	public void setClose(double close) {
		this.close = close;
	}

	public double getWap() {
		return wap;
	}

	public void setWap(double wap) {
		this.wap = wap;
	}

	public double getNoOfShares() {
		return noOfShares;
	}

	public void setNoOfShares(double noOfShares) {
		this.noOfShares = noOfShares;
	}

	public double getNoOfTrades() {
		return noOfTrades;
	}

	public void setNoOfTrades(double noOfTrades) {
		this.noOfTrades = noOfTrades;
	}

	public double getTotalTurnover() {
		return totalTurnover;
	}

	public void setTotalTurnover(double totalTurnover) {
		this.totalTurnover = totalTurnover;
	}

	public double getDeliverableQuantity() {
		return deliverableQuantity;
	}

	public void setDeliverableQuantity(double deliverableQuantity) {
		this.deliverableQuantity = deliverableQuantity;
	}

	public double getPerDelQuanToTrade() {
		return perDelQuanToTrade;
	}

	public void setPerDelQuanToTrade(double perDelQuanToTrade) {
		this.perDelQuanToTrade = perDelQuanToTrade;
	}

	public double getSpreadHighLow() {
		return spreadHighLow;
	}

	public void setSpreadHighLow(double spreadHighLow) {
		this.spreadHighLow = spreadHighLow;
	}

	public double getSpreadCloseOpen() {
		return spreadCloseOpen;
	}

	public void setSpreadCloseOpen(double spreadCloseOpen) {
		this.spreadCloseOpen = spreadCloseOpen;
	}

	public ConsulErrors getError() {
		return error;
	}

	public void setError(ConsulErrors error) {
		this.error = error;
	}

	@Override
	public String toString() {
		return "QuandlDTO [name=" + name + ", quandlCode=" + quandlCode + ", date=" + date + ", open=" + open
				+ ", high=" + high + ", low=" + low + ", close=" + close + ", wap=" + wap + ", noOfShares=" + noOfShares
				+ ", noOfTrades=" + noOfTrades + ", totalTurnover=" + totalTurnover + ", deliverableQuantity="
				+ deliverableQuantity + ", perDelQuanToTrade=" + perDelQuanToTrade + ", spreadHighLow=" + spreadHighLow
				+ ", spreadCloseOpen=" + spreadCloseOpen + ", error=" + error + "]";
	}

}
