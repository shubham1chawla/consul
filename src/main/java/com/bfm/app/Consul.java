package com.bfm.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Consul {
	public static void main(String[] args) {
		SpringApplication.run(Consul.class, args);
	}
}
