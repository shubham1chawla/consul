package com.bfm.app.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.stereotype.Component;

import com.bfm.app.dto.QuandlDTO;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Component
public class QuandlUtils {
	
	public static QuandlDTO buildDTO(String code, String name, String response) throws ParseException {
		
		QuandlDTO data = new QuandlDTO();
		data.setQuandlCode(code);
		
		// Sets name from the database mapped to the quandl code
		data.setName(name);
		
		JsonParser parser = new JsonParser();
		
		// Getting the main json object
		JsonElement jsonTree = parser.parse(response);
		JsonObject jsonObj = jsonTree.getAsJsonObject();
		
		// Extracting the "dataset_data" object
		JsonElement dataset_data = jsonObj.get("dataset_data");
		JsonObject dataset_data_obj = dataset_data.getAsJsonObject();
		
		// Extracting the data relevent data from json
		JsonElement data_in_dataset = dataset_data_obj.get("data");
		JsonArray data_in_dataset_array = data_in_dataset.getAsJsonArray();
		
		// Extracting the most recent element in the record
		JsonElement first_record_element = null;
		try {
			first_record_element = data_in_dataset_array.get(0);
		}
		catch(Exception e) {
			
			// This error occuered when there is no error thrown by Quandl itself.			
			data.setError(ConsulErrors.RESPONSE_EXCEPTION);
			return data;
		}
		JsonArray first_record_array = first_record_element.getAsJsonArray();
	
		// Extracted the last date of updation
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		data.setDate(df.parse(first_record_array.get(0).getAsString()));
		
		// Extracting rest of the fields
		data.setOpen(first_record_array.get(1).getAsDouble());
		data.setHigh(first_record_array.get(2).getAsDouble());
		data.setLow(first_record_array.get(3).getAsDouble());
		data.setClose(first_record_array.get(4).getAsDouble());
		data.setWap(first_record_array.get(5).getAsDouble());
		data.setNoOfShares(first_record_array.get(6).getAsDouble());
		data.setNoOfTrades(first_record_array.get(7).getAsDouble());
		data.setTotalTurnover(first_record_array.get(8).getAsDouble());
		data.setDeliverableQuantity(first_record_array.get(9).getAsDouble());
		data.setPerDelQuanToTrade(first_record_array.get(10).getAsDouble());
		data.setSpreadHighLow(first_record_array.get(11).getAsDouble());
		data.setSpreadCloseOpen(first_record_array.get(12).getAsDouble());
		
		data.setError(null);
		return data;
	}
	
}
