package com.bfm.app.service.impl;

import java.io.IOException;
import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.bfm.app.dao.TickerDao;
import com.bfm.app.dto.QuandlDTO;
import com.bfm.app.service.QuandlFetch;
import com.bfm.app.util.ConsulErrors;
import com.bfm.app.util.HttpUtils;
import com.bfm.app.util.QuandlUtils;

@Service
public class QuandlFetchImpl implements QuandlFetch {
	
	@Value("${consul.quandl.api.key}")
	private String QUANDL_API_KEY;
	
	private final String METHOD = "GET";
	
	@Autowired
	private TickerDao tickerDao;
	
	@Override
	public QuandlDTO getQuandlData(String code) throws IOException {
		final String response = HttpUtils.getResponseText(buildQuandlApiUrl(code), METHOD);
		QuandlDTO data;
		try {
			data = QuandlUtils.buildDTO(code, tickerDao.getTickerByCode(code).getName(), response);
		} catch (ParseException e) {
			QuandlDTO quandlDTO = new QuandlDTO();
			quandlDTO.setError(ConsulErrors.DATE_PARSE_ERROR);
			return quandlDTO;
		}
		return data;
	}
	
	private String buildQuandlApiUrl(String code) {
		StringBuilder sb = new StringBuilder();
		sb.append("https://www.quandl.com/api/v3/datasets/BSE/");
		sb.append(code);
		sb.append("/data.json?&api_key=");
		sb.append(QUANDL_API_KEY);
		return sb.toString();
	}

}
