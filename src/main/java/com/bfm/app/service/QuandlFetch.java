package com.bfm.app.service;

import java.io.IOException;

import com.bfm.app.dto.QuandlDTO;

public interface QuandlFetch {
	public QuandlDTO getQuandlData(String code) throws IOException;
}
